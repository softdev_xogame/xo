/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

/**
 *
 * @author sirapolaya
 */
import java.util.Scanner;

public class LAB1 {
    
    static char[][] Board = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char XO = 'X';
    static String Status = "playing";
    static int row,col;
    static String Win = "0";
    static int count = 0;
    
    static void addXO(){
        Scanner kb = new Scanner(System.in);
        System.out.print("turn "+ XO +" \nPlease input row,col : ");
        row = kb.nextInt(); col = kb.nextInt();
        Board[row][col] = XO;
        count ++;
        if(XO == 'X'){
            XO = 'O';
        }else if(XO == 'O'){
            XO = 'X';
        }
    }
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Welcome To XO Game");
        
        while(Status.equals("playing")){
            for(int i = 0;i<3;i++){
                for(int j = 0;j<3;j++){
                    System.out.print(Board[i][j]+" ");
                }
                System.out.println();
            }
                    if(!"0".equals(Win)){
                        System.out.println(Win+" Win!");
                        System.out.println("continue?(Y/N)");
                        String yn = kb.next();
                            if(yn.equals("y")){
                                for(int i = 0;i<3;i++){
                                for(int j = 0;j<3;j++){
                                Board[i][j] = '-';
                }
            }
                    XO ='X';
                    Win = "0";
                    count = 0;

                    }else if(yn.equals("n")){
                        Status = "game over";
                        System.out.println(Status);
                        break;
                    } 
                    }
                    else if(count == 9){
                        System.out.println("XO Draw!");
                        
                    }
                    
            addXO();
            for(int i =0;i<3;i++){
                if(Board[i][0] == 'X' && Board[i][1] == 'X' && Board[i][2] == 'X'){
                    Win = "X";
                }else if(Board[i][0] == 'O' && Board[i][1] == 'O' && Board[i][2] == 'O'){
                    Win = "O";
                }else if(Board[0][i] == 'X' && Board[1][i] == 'X' && Board[2][i] == 'X'){
                    Win = "X";
                }else if(Board[0][i] == 'O' && Board[1][i] == 'O' && Board[2][i] == 'O'){
                    Win = "O";
                }else if(Board[0][0] == 'X' && Board[1][1] == 'X' && Board[2][2] == 'X'){
                    Win = "X";
                }else if(Board[0][2] == 'X' && Board[1][i] == 'X' && Board[2][0] == 'X'){
                    Win = "X";
                }else if(Board[0][0] == 'O' && Board[1][1] == 'O' && Board[2][2] == 'O'){
                    Win = "O";
                }else if(Board[0][2] == 'O' && Board[1][i] == 'O' && Board[2][0] == 'O'){
                    Win = "O";
                }
            }
        }
    }
    
}
